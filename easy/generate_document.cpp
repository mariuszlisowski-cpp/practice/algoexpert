#include <cassert>
#include <iostream>
#include <unordered_map>
#include <string>

/* O(n + m) time | O(c) space 
   n - number of characters
   m - length of the document
   c - number of unique characters */
bool generateDocument(std::string characters, std::string document) {
    std::unordered_map<char, int> characters_map;
    for (const auto ch : characters) {
        ++characters_map[ch];
    }
    for (const auto ch : document) {
        if (characters_map.find(ch) == characters_map.end() ||
            characters_map[ch] == 0)
        {
            return false;
        } else {
            --characters_map[ch];
        }
    }

    return true;
}

int main() {
    auto characters = "Bste!hetsi ogEAxpelrt x ";
    auto document = "AlgoExpert is the Best!";
    auto expected = true;
    auto actual = generateDocument(characters, document);
    
    assert(expected == actual);

    return 0;
}
