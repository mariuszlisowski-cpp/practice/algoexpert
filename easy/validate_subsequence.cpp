#include <iostream>
#include <vector>

/*  time    O(n) one traveral (n is the length of the array)
    space   O(1)
*/
bool isValidSubsequence(const std::vector<int>& array, const std::vector<int>& sequence) {
    auto seq_it = sequence.begin();
    for (auto el : array) {
        if (el == *seq_it) {
            if (seq_it  == sequence.end() - 1) {
                return true;
            } else {
                ++seq_it;
            }
        }
    }

    return false;
}

int main() {
    std::vector<int> array{5, 1, 22, 25, 6, -1, 8, 10, 2};
    std::vector<int> sequence{1, 6, -1, 10};

    std::cout << (isValidSubsequence(array, sequence) ? "valid" : "NOT valid") << std::endl;

    return 0;
}
