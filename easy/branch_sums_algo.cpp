#include <iostream>
#include <memory>
#include <vector>

class BinaryTree {
  public:
    int value;
    BinaryTree* left;
    BinaryTree* right;

    BinaryTree(int value) {
        this->value = value;
        left = nullptr;
        right = nullptr;
    }
};

class TestBinaryTree : public BinaryTree {
  public:
    TestBinaryTree(int value) : BinaryTree(value){};

    BinaryTree* insert(std::vector<int> values, int i = 0) {
        if (i >= values.size())
            return nullptr;
        std::vector<BinaryTree*> queue = {this};
        while (queue.size() > 0) {
            BinaryTree* current = queue[0];
            queue.erase(queue.begin());
            if (current->left == nullptr) {
                current->left = new BinaryTree(values[i]);
                break;
            }
            queue.push_back(current->left);
            if (current->right == nullptr) {
                current->right = new BinaryTree(values[i]);
                break;
            }
            queue.push_back(current->right);
        }
        insert(values, i + 1);
        return this;
    }
};

void calculateBranchSums(BinaryTree *node, int runningSum, std::vector<int> &sums);

// O(n) time | O(n) space - where n is the number of nodes in the Binary Tree
std::vector<int> branchSums(BinaryTree *root) {
  std::vector<int> sums;
  calculateBranchSums(root, 0, sums);
  return sums;
}

void calculateBranchSums(BinaryTree *node, int runningSum, std::vector<int> &sums) {
  if (node == nullptr)
    return;

  int newRunningSum = runningSum + node->value;
  if (node->left == nullptr && node->right == nullptr) {
    sums.push_back(newRunningSum);
    return;
  }

  calculateBranchSums(node->left, newRunningSum, sums);
  calculateBranchSums(node->right, newRunningSum, sums);
}

int main() {

    TestBinaryTree* tree = new TestBinaryTree(1);
    tree->insert({2, 3, 4, 5, 6, 7, 8, 9, 10});
    std::vector<int> expected = {15, 16, 18, 10, 11};
    assert(branchSums(tree) == expected);

    tree = new TestBinaryTree(1);
    expected = {1};
    assert(branchSums(tree) == expected);

    return 0;
}
