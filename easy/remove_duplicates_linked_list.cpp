#include <cassert>
#include <vector>

class LinkedList {
public:
    int value;
    LinkedList* next = nullptr;

    LinkedList(int value) { this->value = value; }
};

LinkedList* addMany(LinkedList* linkedList, std::vector<int> values) {
    LinkedList* current = linkedList;
    while (current->next != nullptr) {
        current = current->next;
    }
    for (auto value : values) {
        current->next = new LinkedList(value);
        current = current->next;
    }
    return linkedList;
}

std::vector<int> getNodesInArray(LinkedList* linkedList) {
    std::vector<int> nodes;
    LinkedList* current = linkedList;
    while (current != nullptr) {
        nodes.push_back(current->value);
        current = current->next;
    }
    return nodes;
}

// O(n) time | O(1) space, n - number of nodes
LinkedList* removeDuplicatesFromLinkedList(LinkedList* linkedList) {
    LinkedList* head{ linkedList };
    if (linkedList) {
        int prev_val{ linkedList->value };
        LinkedList* prev_ptr{ linkedList };
        linkedList = linkedList->next;
        while (linkedList) {
            if (prev_val == linkedList->value) {
                prev_ptr->next = linkedList->next;
                delete linkedList;
                linkedList = prev_ptr->next;
            } else {
                prev_val = linkedList->value;
                prev_ptr = linkedList;
                linkedList = linkedList->next;
            }
        }
    }

    return head;
}

int main() {
    auto input = addMany(new LinkedList(1), std::vector<int>{ 1, 3, 4, 4, 4, 5, 6, 6 });
    auto expected = addMany(new LinkedList(1), std::vector<int>{ 3, 4, 5, 6 });
    auto actual = removeDuplicatesFromLinkedList(input);

    assert(getNodesInArray(actual) == getNodesInArray(expected)); // 1, 2, 3, 4, 5, 6

    return 0;
}
