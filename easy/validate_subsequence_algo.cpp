#include <iostream>
#include <vector>

// O(n) time | O(1) space - where n is the length of the array
bool isValidSubsequence(std::vector<int> array, std::vector<int> sequence) {
  int arrIdx = 0;
  int seqIdx = 0;
  while (arrIdx < array.size() && seqIdx < sequence.size()) {
    if (array[arrIdx] == sequence[seqIdx]) {
      seqIdx++;
    }
    arrIdx++;
  }
  
  return seqIdx == sequence.size();
}

int main() {
    std::vector<int> array{5, 1, 22, 25, 6, -1, 8, 10, 2};
    std::vector<int> sequence{1, 6, -1, 10};

    std::cout << (isValidSubsequence(array, sequence) ? "valid" : "NOT valid") << std::endl;

    return 0;
}
