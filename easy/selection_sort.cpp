#include <assert.h>
#include <iostream>
#include <utility>
#include <vector>

// average O(n^2) time | O(1) space
std::vector<int> selectionSort(std::vector<int> array) {
    if (array.empty()) {
        return {};
    }
    size_t j{};
    while (j < array.size()) {
        size_t min_idx{ j };
        for (size_t i{ j + 1 }; i < array.size(); ++i) {
            if (array[i] < array[min_idx]) {
                min_idx = i;
            }
        }
        std::swap(array[j], array[min_idx]);
        ++j;
    }

    return array;
}

int main() {
    std::vector<int> unsorted{ 8, 5, 2, 9, 5, 6, 3 };
    std::vector<int> sorted{ 2, 3, 5, 5, 6, 8, 9 };

    assert(selectionSort(unsorted) == sorted);

    return 0;
}
