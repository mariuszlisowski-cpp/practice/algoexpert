#include <iostream>

/* time O(2^n) | space O(n) */
int getNthFib(int n) {
    if (n == 1) {
        return 0;
    }
    if (n == 2) {
        return 1;
    }

    return getNthFib(n - 1) + getNthFib(n - 2); // unnecessary repetetive calculations 
}

int main() {
    const int nth_fibonacci = 8;

    std::cout << nth_fibonacci << "th Fibonacci number: " << getNthFib(nth_fibonacci) << std::endl;

    return 0;
}

/* 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 seq
   1  2  3  4  5  6  7   8   9  10  11  12 th */
