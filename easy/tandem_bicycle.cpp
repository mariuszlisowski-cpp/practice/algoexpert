#include <algorithm>
#include <cassert>
#include <vector>

int tandemBicycle(std::vector<int> redShirtSpeeds, std::vector<int> blueShirtSpeeds,
                  bool fastest)
{
    std::sort(redShirtSpeeds.begin(), redShirtSpeeds.end());
    if (fastest) {
        std::sort(blueShirtSpeeds.begin(), blueShirtSpeeds.end(), std::greater());
    } else {
        std::sort(blueShirtSpeeds.begin(), blueShirtSpeeds.end(), std::less{});
    }
    int total{};
    for (size_t i{}; i < redShirtSpeeds.size(); ++i) {
        total += std::max(redShirtSpeeds[i], blueShirtSpeeds[i]);
    }

    return total;
}

int main() {
    auto redShirtSpeeds = {5, 5, 3, 9, 2};
    auto blueShirtSpeeds = {3, 6, 7, 2, 1};
    
    // auto fastest = false;
    // auto expected = 25;
    
    auto fastest = true;
    auto expected = 32;
    
    auto actual = tandemBicycle(redShirtSpeeds, blueShirtSpeeds, fastest);
    
    assert(expected == actual);

    return 0;
}
