#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

int minimumWaitingTime(std::vector<int> queries) {
    std::sort(queries.begin(), queries.end());
    int time{};
    int total{};
    for (int i = 0; i < queries.size() - 1; ++i) {
        total += time += queries[i];
    }

    return total;
}

int main() {
    std::vector<int> queries = { 3, 2, 1, 2, 6 };
    int expected = 17;
    auto actual = minimumWaitingTime(queries);
    
    assert(expected == actual);

    return 0;
}
