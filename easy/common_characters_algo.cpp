#include <cassert>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>

/*  Write a function that takes in a non-empty list of non-empty strings and
    returns a list of characters that are common to all strings in the list,
    ignoring multiplicity.

    Note that the strings are not guaranteed to only contain alphanumeric characters.
    The list you return can be in any order.
*/

std::vector<std::string> common_characters(std::vector<std::string> strings) {
    if (strings.size() == 0) {
        return {};
    }
    
    std::unordered_map<char, std::size_t> occurences;
    for (auto& str : strings) {
        std::unordered_set<char> uniques{str.begin(), str.end()};                   // set stores unique values only             !!!
        for (const auto ch : uniques) {
            ++occurences[ch];
        }
    }

    /* output of single character occurences */
    for (auto [ch, occurence] : occurences) {
        std::cout << ch << ':' << occurence << ' ';
    }

    std::vector<std::string> results;
    std::for_each(occurences.begin(), occurences.end(),
                  [&](auto& pair) {
                      if (strings.size() == pair.second) {                          // char seen in all strings (size of vector)
                          results.push_back(std::string{pair.first});               // convert char to string
                      }
                  });

    /* list of characters that are common to all strings */
    std::copy(results.begin(), results.end(),
              std::ostream_iterator<std::string>(std::cout, " "));

    return results;
}

int main() {
        std::vector<std::string> strings{"abc", "bcd", "cbaccd"};
        std::vector<std::string> expected{"b", "c"};                                // the characters could be ordered differently

    common_characters(strings);

    return 0;
}
