#include <assert.h>
#include <iostream>
#include <utility>
#include <vector>

// average O(n^2) time | O(1) space
std::vector<int> insertionSort(std::vector<int> array) {
    if (array.empty()) {
        return {};
    }
    for (int i{ 1 }; i < array.size(); ++i) {
        int j{ i };
        while (j > 0 && array[j] < array[j - 1]) {
            std::swap(array[j], array[j - 1]);
            --j;
        }
    }

    return array;
}

int main() {
    std::vector<int> unsorted{ 8, 5, 2, 9, 5, 6, 3 };
    std::vector<int> sorted{ 2, 3, 5, 5, 6, 8, 9 };

    assert(insertionSort(unsorted) == sorted);

    return 0;
}
