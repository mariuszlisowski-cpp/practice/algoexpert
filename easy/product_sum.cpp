#include <any>
#include <iostream>
#include <vector>

/*  Tip: You can use el.type() == typeid(vector<any>) to check whether an item is a list or an integer.
    If you know an element of the array is vector<any> you can cast it using:
    > any_cast<vector<any>>(element)
    If you know an element of the array is an int you can cast it using:
    > any_cast<int>(element)
 */

// time O(n) here: n = 12 el | space O(d) here: 3 (depth of subarrays )
int productSum(std::vector<std::any> array, int multiplier = 1) {
    int sum{};

    for (const auto& el : array) {
        if (el.type() == typeid(std::vector<std::any>)) {
            auto el_array = std::any_cast<std::vector<std::any>>(el);
            sum += productSum(el_array, multiplier + 1);
        } else if (el.type() == typeid(int)) {
            auto el_any = std::any_cast<int>(el);
            sum += el_any;
        }
    } 

    return sum * multiplier;
}

int main() {
    // nested array [ 5, 2, [7, -1], 3, [6, [-13, 8], 4] ] // 9 elements + 3 nested = 12 elements
    // calculated as: 5 + 2 + 2*(7 - 1) + 3 + 2*(6 + 3*(-13 + 8) + 4)
    //                    7 + 12        + 3 + 2*(6 - 15 + 4)
    //                        19        + 3 + 2*(-5)
    //                        19        + 3 - 10     = 12
    std::vector<std::any> test = {5, 2, std::vector<std::any>{7, -1}, 3,
                                  std::vector<std::any>{6, std::vector<std::any>{-13, 8}, 4}}; // 12

    std::cout << productSum(test);

    return 0;
}
