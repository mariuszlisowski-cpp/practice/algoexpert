#include <algorithm>
#include <iostream>
#include <vector>

void print(std::vector<int> v) {
    for (auto el : v) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

bool classPhotos(std::vector<int> redShirtHeights, std::vector<int> blueShirtHeights) {
    if (redShirtHeights.size() != blueShirtHeights.size()) {
        return false;
    }
    std::sort(std::begin(redShirtHeights), std::end(redShirtHeights), std::greater());
    std::sort(std::begin(blueShirtHeights), std::end(blueShirtHeights), std::greater());

    /* verbose */
    print(redShirtHeights);
    print(blueShirtHeights);

    if (blueShirtHeights[0] < redShirtHeights[0]) {
        std::swap(blueShirtHeights, redShirtHeights);
    }
    for (size_t i = 0; i < redShirtHeights.size(); i++) {
        if (blueShirtHeights[i] <= redShirtHeights[i]) {
            return false;
        } 
    }

    return true;
}

int main() {
    std::vector<int> red{5, 8, 1, 3, 4};                                // 1st row
    std::vector<int> blue{6, 9, 2, 4, 5};                               // 2nd row

    std::cout << std::boolalpha << classPhotos(red, blue) << std::endl; // true


    
    return 0;
}
