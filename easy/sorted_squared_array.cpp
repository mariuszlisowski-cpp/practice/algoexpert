#include <algorithm>
#include <cassert>
#include <vector>

// brute force solution (not optimial)
// O(nlogn) time | O(n) space, n - length of the input array
std::vector<int> sortedSquaredArray(std::vector<int> array) {
    for (auto& el : array) {
        el *= el;
    }
    std::sort(array.begin(), array.end());

    return array;
}

int main() {
    std::vector<int> input = { -2, -1 };
    std::vector<int> expected = { 1, 4 };
    auto actual = sortedSquaredArray(input);
    
    assert(expected == actual);

    return 0;
}
