/* 
You're given a 2D array of integers matrix. Write a function that returns the transpose of the matrix.

The transpose of a matrix is a flipped version of the original matrix across its main diagonal
(which runs from top-left to bottom-right); it switches the row and column indices of the original matrix.

You can assume the input matrix always has at least 1 value; however its width and height are not necessarily the same.

# Input #1         |             # Input #2D         |            # Input #3
matrix = [         |             matrix = [          |            matrix = [
  [1, 2],          |               [1, 2],           |              [1, 2, 3],
]                  |               [3, 4],           |              [4, 5, 6],
                   |               [5, 6]            |              [7, 8, 9]
                   |             ]                   |            ]

Output #1          |             Output #2D          |            Output #3
[                  |            [                    |           [
  [1],             |              [1, 3, 5],         |             [1, 4, 7],
  [2]              |               [2, 4, 6]         |              [2, 5, 8],
]                  |             ]                   |              [3, 6, 9]
                   |                                 |            ]
*/
#include <iostream>
#include <cassert>
#include <vector>

std::vector<std::vector<int>> transposeMatrix(std::vector<std::vector<int>> matrix) {
    auto rows = matrix.size();
    if (rows == 0) {
        return {};
    }
    auto cols = matrix[0].size();
    if (cols == 0) {
        return {{}};
    }

    std::vector<std::vector<int>> output(cols, std::vector<int>(rows));
    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < cols; ++j) {
            output[j][i] = matrix[i][j];
        }
    }

    return output;
}

template <typename T>
void print2D(const T& t) {
    for (int row{ 0 }; row < t.size(); ++row) {
        for (int col{ 0 }; col < t[row].size(); ++col) {
            std::cout << t[row][col] << ' ' << std::flush;
        }
        std::cout << std::endl;
    }
}

int main() {
    // std::vector<std::vector<int>> input = {};                               // rows == 0
    // std::vector<std::vector<int>> input = {{}};                             // cols == 0
    // std::vector<std::vector<int>> input = {{1}};                            // one element
    // std::vector<std::vector<int>> input = {{1}, {2}, {3}};                  // one row
    // std::vector<std::vector<int>> input = {{1, -1}};                        // one column
    std::vector<std::vector<int>> input = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    std::vector<std::vector<int>> expected = {{1, 4, 7}, {2, 5, 8}, {3, 6, 9}};

    std::cout << "# Input :" << std::endl;
    print2D(input);

    auto actual = transposeMatrix(input);

    std::cout << "# Output:" << std::endl;
    print2D(actual);
    assert(expected == actual);
    
    return 0;
}
