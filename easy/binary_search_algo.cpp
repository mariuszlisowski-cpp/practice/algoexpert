#include <cassert>
#include <iostream>
#include <vector>

// one more iteration needed sometimes to find the last element
int binary_search(std::vector<int> array, int target, int begin, int end) {
	if (begin <= end) {
        int mid = (begin + end) / 2;
        
        std::cout << begin << ' ' << end << ' ' << mid << std::endl;
        
        if (target == array[mid]) {
            return mid;
        } else if (target < array[mid]) {
            return binary_search(array, target, begin, mid - 1);
        } else if (target > array[mid]) {
            return binary_search(array, target, mid + 1, end);
        }
    }

    return -1;
}

int binarySearch(std::vector<int> array, int target) {
    return binary_search(array, target, 0, array.size() - 1);
}

int main() {
    std::vector<int> v{ 0, 1, 21, 33, 45, 45, 61, 71, 72, 73 };
    int target{ 72 };                                                       // whether is contained in an array
    int result{ 8 };                                                        // index of contained integer

    assert(binarySearch(v, target) == result);                              // needs 4 iterations

    return 0;
}
