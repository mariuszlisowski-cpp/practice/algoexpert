#include <cstddef>
#include <iostream>
#include <memory>
#include <vector>

class BinaryTree {
  public:
    int value;
    BinaryTree* left;
    BinaryTree* right;

    BinaryTree(int value) {
        this->value = value;
        left = nullptr;
        right = nullptr;
    }
};

int nodeDepths(BinaryTree *root,
               int depth = 0,
               const std::unique_ptr<int>& sum = std::make_unique<int>())
{
    if (root) {
        *sum.get() += depth;
        if (root->left) {
            nodeDepths(root->left, depth + 1, sum);
        }
        if (root->right) {
            nodeDepths(root->right, depth + 1, sum);
        }
    }
 
    return *sum.get();
}

int main() {
    BinaryTree* root = new BinaryTree(1);
    root->left = new BinaryTree(2);
    root->left->left = new BinaryTree(4);
    root->left->left->left = new BinaryTree(8);
    root->left->left->right = new BinaryTree(9);
    root->left->right = new BinaryTree(5);
    root->right = new BinaryTree(3);
    root->right->left = new BinaryTree(6);
    root->right->right = new BinaryTree(7);
    int actual = nodeDepths(root);
    assert(actual == 16);

    return 0;
}
