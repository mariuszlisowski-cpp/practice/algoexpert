#include <iostream>
#include <string>
#include <vector>

// O(n) time | O(n) space
std::string caesarCypherEncryptor(std::string str, int key) {
    const int ALPHABET_LETTERS_COUNT{ 26 };
    const char ASCII_LOWERCASE_Z{ 122 };

    auto shift{ key % ALPHABET_LETTERS_COUNT };
    if (!shift) {
        return str;
    }

    std::string output;
    for (char letter : str) {
        int shifted{ letter + shift };
        if (shifted > ASCII_LOWERCASE_Z) {
            shifted -= ALPHABET_LETTERS_COUNT;
        }
        output += static_cast<char>(shifted);
    }

    return output;
}

int main() {
    auto str{ caesarCypherEncryptor("xyz", 52) };    // whole shifted twice thus no shift

    std::cout << str << std::endl;

    return 0;
}
