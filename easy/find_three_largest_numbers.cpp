#include <algorithm>
#include <climits>
#include <iostream>
#include <limits>
#include <vector>

// no sorting allowed (duplicates allowed)
std::vector<int> findThreeLargestNumbers(std::vector<int> array) {
    const int LARGEST_NUMS_COUNT{ 3 };
    const int LAST_INDEX = LARGEST_NUMS_COUNT - 1;
    const int MIDDLE_INDEX = LARGEST_NUMS_COUNT - 2;
    const int FIRST_INDEX = LARGEST_NUMS_COUNT - 3;
    
    std::vector<int> result(LARGEST_NUMS_COUNT);
    std::fill(result.begin(), result.end(), std::numeric_limits<int>::min());
    
    for (auto el : array) {
        if (el > result[LAST_INDEX]) {
            result[FIRST_INDEX] = result[MIDDLE_INDEX];     // shift middle to first
            result[MIDDLE_INDEX] = result[LAST_INDEX];      // shift last to middle
            result[LAST_INDEX] = el;
        } else if (el > result[MIDDLE_INDEX]) {
            result[FIRST_INDEX] = result[MIDDLE_INDEX];     // shift middle to first
            result[MIDDLE_INDEX] = el;
        } else if (el > result[FIRST_INDEX]) {
            result[FIRST_INDEX] = el;                       // no need to shift
        }
    }
    
    return result;
}

void print_vector(std::vector<int> v) {
    for (auto el : v) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v1{141, 1,   17, -7, -17, -27, 18,  541, 8,  7,  7}; // 18, 141, 541
    std::vector<int> v2{10, 5, 9, 10, 12};                                // 10, 10, 12

    auto resultA{findThreeLargestNumbers(v1)};
    print_vector(resultA);

    auto resultB{findThreeLargestNumbers(v2)};
    print_vector(resultB);

    return 0;
}
