#include <cassert>
#include <iostream>
#include <string>
#include <unordered_map>

// O(n) time | O(1) space, n length of the input str
int firstNonRepeatingCharacter(std::string string) {
    if (!string.empty()) {
        std::unordered_map<char, int> letters;
        for (const auto ch : string) {
            ++letters[ch];
        }
        for (int i = 0; i < string.size(); ++i) {
            if (letters[string[i]] == 1) {
                return i;
            }
        }
    }

    return -1;
}

int main() {
    auto input = "abcdcaf";                             // first non-repeating: 'b'
    auto expected = 1;                                  // at index: 1
    auto actual = firstNonRepeatingCharacter(input);
    
    assert(expected == actual);

    return 0;
}
