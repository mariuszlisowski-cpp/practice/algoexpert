#include <iostream>
#include <unordered_set>
#include <vector>

// O(n) time | O(n) space
std::vector<int> twoNumberSum(const std::vector<int>& array, int targetSum) {
    std::unordered_set<int> nums;
    for (int num : array) {
        int potentialMatch = targetSum - num;
        if (nums.find(potentialMatch) != nums.end()) {
            return std::vector<int>{ potentialMatch, num };
        } else {
            nums.insert(num);
        }
    }

    return {};
}

int main() {
    std::vector<int> res = twoNumberSum({ 3, 5, -4, 8, 11, 1, -1, 6 }, 10);  // 11, -1

    for (auto el : res) {
        std::cout << el << std::endl;
    }

    return 0;
}
