#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

// O(nlogn) time | O(1) space
int nonConstructibleChange(vector<int> coins) {
    sort(coins.begin(), coins.end());
    int change{};
    for (auto coin : coins) {
        if (coin > change + 1) {
            return change + 1;
        }
        change += coin;
    }

    return change + 1;
}

int main() {
    vector v{5, 7, 1, 1, 2, 3, 22}; // 20

    std::cout << nonConstructibleChange(v) << std::endl;
}
