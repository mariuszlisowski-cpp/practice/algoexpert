#include <algorithm>
#include <cassert>
#include <vector>
#include <string>

/* Given an array of buildings and a direction that all of the buildings face, return an array of the indices of the buildings that
can see the sunset. A building can see the sunset if it's strictly taller than all of the buildings that come after it in the
direction that it faces. The input array named buildings contains positive, non-zero integers representing the heights of the
buildings. A building at index i thus has a height denoted by buildings[i]. All of the buildings face the same direction,
and thisdirection is either east or west, denoted by the input string named direction, which will always be equal to either
"EAST" or "WEST". In relation to the input array, you can interpret these directions as right for east and left for west.
Important note: the indices in the ouput array should be sorted in ascending order.

Sample Input #1:

buildings = [3, 5, 4, 4, 3, 1, 3, 2]
direction = "EAST"

Sample Output #1: [1, 3, 6, 7]

// Below is a visual representation of the sample input.
//    _
//   | |_ _
//  _| | | |_   _
// | | | | | | | |_
// | | | | | |_| | |
// |_|_|_|_|_|_|_|_|

Sample Input #2:
buildings = [3, 5, 4, 4, 3, 1, 3, 2]
direction = "WEST"

Sample Output #2: [0, 1]
The buildings are the same as in the first sample input, but their direction is reversed.
*/

// Codeium translated from Python source
// O(n) time | O(n) space
std::vector<int> sunsetViews(std::vector<int> buildings, std::string direction) {
    if (buildings.empty()) {
        return {};
    }

    int arrLen = buildings.size();
    std::vector<int> output;
    int max = 0;

    for (int i = (direction == "EAST") ? arrLen - 1 : 0;
         (direction == "EAST") ? i >= 0 : i < arrLen;
         (direction == "EAST") ? i-- : i++) {
        if (buildings[i] > max) {
            output.push_back(i);
            max = buildings[i];
        }
    }

    if (direction == "EAST") {
        std::reverse(output.begin(), output.end());
    }

    return output;
}


int main() {
    std::vector<int> buildings{3, 5, 4, 4, 3, 1, 3, 2};
    {
        std::string direction = "EAST";
        std::vector<int> expected{1, 3, 6, 7};
        std::vector<int> actual = sunsetViews(buildings, direction);

        assert(expected == actual);
    }
    {
        std::string direction = "WEST";
        std::vector<int> expected{0, 1};
        std::vector<int> actual = sunsetViews(buildings, direction);

        assert(expected == actual);
    }

    return 0;
}
