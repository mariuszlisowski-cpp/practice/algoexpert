#include <cassert>
#include <iostream>
#include <stack>
#include <string>

// time O(n); space O(n)
bool balancedBrackets(const std::string& str) {
    std::stack<char> st;
    
    for (auto ch : str) {
        switch (ch) {
            case '(': st.push(')'); break;
            case '[': st.push(']'); break;
            case '{': st.push('}'); break;

            case ')':
            case ']':
            case '}':
                if (!st.empty() && ch == st.top()) {
                    st.pop();
                } else {
                    return false;
                }
        }
    }

    return st.empty();
}

int main() {
    
    auto areBracketsBalanced = []() { assert(balancedBrackets("([])(){}(())()()") == true); };
    areBracketsBalanced();
    
    return 0;
}
