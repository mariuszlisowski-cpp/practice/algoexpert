#include <algorithm>
#include <cassert>
#include <iostream>
#include <limits>
#include <vector>

void print(std::vector<int> v) {
    std::cout << "> sorted: ";
    for (auto el : v) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

// O(nlog(n) + mlog(m)) time | O(1) space
// n - number of elements of the first array
// m - number of elements of the second array
std::vector<int> smallestDifference(std::vector<int> arrayOne, std::vector<int> arrayTwo) {
    std::sort(arrayOne.begin(), arrayOne.end());
    std::sort(arrayTwo.begin(), arrayTwo.end());

    print(arrayOne);
    print(arrayTwo);

    std::vector<int> pair(2);
    auto itOne{ arrayOne.begin() };
    auto itTwo{ arrayTwo.begin() };
    int smallest{ std::numeric_limits<int>::max() };
    while (itOne != arrayOne.end() && itTwo != arrayTwo.end()) {
        auto abs_diff{ std::abs(*itOne - *itTwo) };                     // absolute difference
        if (abs_diff < smallest) {
            smallest = abs_diff;
            pair.front() = *itOne;
            pair.back() = *itTwo;
        }
        if (abs_diff == 0) {
            break;                                                      // found!
        } else {
            if (*itOne < *itTwo) {                                      // increment smaller of two:
                ++itOne;                                                // a need of a bigger number to subtract
            } else {                                                    // ..to find a better pair
                ++itTwo;                                                // ..which difference is smaller
            }
        }
    }

    std::cout << pair.front() << '-' << pair.back() << '=' << smallest;

    return pair;
}

int main() {
    std::vector<int> expected{28, 26};                                  // 28 - 26 = |2|

    assert(smallestDifference({-1, 5, 10, 20, 28, 3},
                              {26, 134, 135, 15, 17}) == expected);
 
     return 0;
}
