#include <cassert>
#include <vector>

std::vector<int> spiralTraverse(std::vector<std::vector<int>> array) {
    /* indices */
    int row_beg = 0;
    int row_end = array.size() - 1;
    int col_beg = 0;
    int col_end = array[0].size() - 1;                          // 0 or any other row as of same length

    std::vector<int> result;
    result.reserve(array.size() * array[0].size());

    while (row_beg <= row_end && col_beg <= col_end) {
        for (int col = col_beg; col <= col_end; ++col) {
            result.push_back(array[row_beg][col]);
        }
        for (int row = row_beg + 1; row <= row_end; ++row) {
            result.push_back(array[row][col_end]);
        }
        for (int col = col_end - 1; col >= col_beg; --col) {
            if (row_beg == row_end) {
                break;                                          // if single row (no double count)
            }
            result.push_back(array[row_end][col]);
        }
        for (int row = row_end - 1; row > row_beg; --row) {
            if (col_beg == col_end) {
                break;                                          // if single column (no double count)
            }
            result.push_back(array[row][col_beg]);
        }

        ++row_beg;
        --row_end;
        ++col_beg;
        --col_end;
    }
   
    return result;
}

int main() {
    std::vector<std::vector<int>> input = {
          { 1,  2,  3, 4},
          {12, 13, 14, 5},
          {11, 16, 15, 6},
          {10,  9,  8, 7},
      };
      std::vector<int> expected = {1, 2,  3,  4,  5,  6,  7,  8,
                                   9, 10, 11, 12, 13, 14, 15, 16};
      std::vector<int> actual = spiralTraverse(input);
      assert(expected == actual);

}
