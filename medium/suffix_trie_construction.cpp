#include <cassert>
#include <unordered_map>
#include <string>

class TrieNode {
public:
    std::unordered_map<char, TrieNode *> children;
};

class SuffixTrie {
public:
    TrieNode *root;
    char endSymbol;

    SuffixTrie(std::string str) {
        this->root = new TrieNode();
        this->endSymbol = '*';
        this->populateSuffixTrieFrom(str);
    }

    void populateSuffixTrieFrom(std::string str) {
        auto node = this->root;
        for (int i{0}; i < str.size(); i++) {
            node = this->root;
            for (const auto& ch : str.substr(i, str.size())) {
                if (!node->children.contains(ch)) {                                     // contains (c++20)
                    node->children[ch] = new TrieNode();
                }
                node = node->children[ch];
            }
            node->children[endSymbol] = nullptr;
        }
    }

    bool contains(std::string str) {
        auto node = this->root;
        for (const auto& ch : str) {
            if (!node->children.count(ch)) {                                            // saa using count
                return false;
            }
            node = node->children[ch];
        }
        return node->children.find(this->endSymbol) != node->children.end();            // saa using find
    }
};

int main() {
    std::string word = "babc";
    SuffixTrie actual(word);
    assert(actual.contains("abc") == true);
    assert(actual.contains("bc") == true);
    assert(actual.contains("c") == true);
    assert(actual.contains("bab") == false);                                            // not a suffix

    return 0;
}
