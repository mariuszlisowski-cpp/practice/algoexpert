#include <cassert>
#include <vector>

std::vector<int> moveElementToEnd(std::vector<int> array, int toMove) {
    if (!array.empty()) {
        auto it_left{array.begin()};
        auto it_right{array.end() - 1};
        while (it_left < it_right) {
            if (*it_left == toMove) {
                while (*it_right == toMove) {
                    --it_right;
                }
                if (it_left < it_right) {
                    std::swap(*it_left, *it_right);
                    --it_right;
                }
            }
            ++it_left;
        }
    }

    return array;
}

int main() {
    std::vector<int> array = {2, 1, 2, 2, 2, 3, 4, 2};
    int toMove = 2;
    std::vector<int> expectedStart = {1, 3, 4};
    std::vector<int> expectedEnd = {2, 2, 2, 2, 2};
    std::vector<int> output = moveElementToEnd(array, toMove);
    assert(output.size() == array.size());
    std::vector<int> outputStart =
        std::vector<int>(output.begin(), output.begin() + 3);
    sort(outputStart.begin(), outputStart.end());
    std::vector<int> outputEnd =
        std::vector<int>(output.begin() + 3, output.end());
    assert(outputStart == expectedStart);
    assert(outputEnd == expectedEnd);

    return 0;
}
