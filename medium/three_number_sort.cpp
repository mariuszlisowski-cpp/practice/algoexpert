#include <algorithm>
#include <vector>

// time O(n)
std::vector<int> threeNumberSort(std::vector<int> array, std::vector<int> order) {
    std::vector<int> result;
    result.reserve(array.size());
    for (const auto& el : order) {
        result.insert(result.end(),
                      std::count(array.begin(), array.end(), el),
                      el);
    }
    return result;
}

int main() {
    std::vector<int> array = {1, 0, 0, -1, -1, 0, 1, 1};
    std::vector<int> order = {0, 1, -1};
    std::vector<int> expected = {0, 0, 0, 1, 1, 1, -1, -1};
    
    auto actual = threeNumberSort(array, order);
    assert(expected == actual);
}
