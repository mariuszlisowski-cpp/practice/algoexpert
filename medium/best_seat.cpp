#include <cassert>
#include <cstddef>
#include <vector>

auto best_seat(const std::vector<int>& seats) {
    size_t best_seat_index = -1;
    size_t max_consecutive_empty_seats = 0;
    size_t current_consecutive_empty_seats = 0;
    for (size_t idx = 0; idx < seats.size(); ++idx) {
        if (seats[idx] == 0) {
            ++current_consecutive_empty_seats;
            if (current_consecutive_empty_seats > max_consecutive_empty_seats) {
                best_seat_index = idx - current_consecutive_empty_seats / 2;
                max_consecutive_empty_seats = current_consecutive_empty_seats;
            }
        } else {
            current_consecutive_empty_seats = 0;
        }
    }
    return best_seat_index;
}

int main() {
    const std::vector<int> input = {1, 0, 1, 0, 0, 0, 1};
    const int expected = 4;
    const size_t actual = best_seat(input);
    assert(expected == actual);

    return 0;
}
