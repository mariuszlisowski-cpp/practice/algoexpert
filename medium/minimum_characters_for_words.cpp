#include <algorithm>
#include <vector>
#include <unordered_map>
#include <string>

std::vector<char> minimumCharactersForWords(std::vector<std::string> words) {
    std::unordered_map<char, unsigned> occurences;
    std::unordered_map<char, unsigned> results;
    for (const auto& word : words) {
        for (const auto& ch : word) {
            ++occurences[ch];
        }
        std::for_each(occurences.begin(), occurences.end(),
                      [&](auto& occurence) {
                           if (results.count(occurence.first) == 0 ||
                               results[occurence.first] < occurence.second)
                           {
                               results[occurence.first] = occurence.second;
                           }
                       });
        occurences.clear();
    }

    std::vector<char> output;
    std::for_each(results.begin(), results.end(),
                  [&output](const auto& result) {
                      for (auto i{0}; i < result.second; ++i) {
                          output.push_back(result.first);
                      }
                  });

    return output;
}

int main() {
    std::vector<std::string> input = {"!!!2", "234", "222", "432"};
    std::vector<char> expected = {'2', '2', '2', '!', '!', '!', '3', '4'};

    auto actual = minimumCharactersForWords(input);
    std::sort(actual.begin(), actual.end());
    std::sort(expected.begin(), expected.end());

    assert(expected == actual);
}
