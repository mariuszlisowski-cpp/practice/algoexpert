/*
Write a function that takes in a non-empty array of arbitrary intervals, merges any overlapping intervals, and returns the new
intervals in no particular order. Each interval 'interval' is an array of two integers, with 'interval[0]' as the start of the
interval and 'interval[1]' as the end of the interval.

Note that back-to-back intervals aren't considered to be overlapping. For example, [1, 5] and [6, 7] aren't overlapping;
however, [1, 6] and [6, 7] are indeed overlapping.

Also note that the start of any particular interval will always be less than or equal to the end of that interval.

Sample Input: intervals = [[1, 2], [3, 5], [4, 7], [6, 8], [9, 10]]

Sample Output: [[1, 2], [3, 8], [9, 10]]  // Merge the intervals [3, 5], [4, 7], and [6, 8].
                                          // The intervals could be ordered differently.
*/
#include <algorithm>
#include <vector>
#include <cassert>

std::vector<std::vector<int>> mergeOverlappingIntervals(std::vector<std::vector<int>> intervals);

class Test{
 public:
  void run() {
      std::vector<std::vector<int>> intervals = {{9, 10}, {1, 2}, {3, 5}, {4, 7}, {6, 8}}; // unsorted
      std::vector<std::vector<int>> expected = {{1, 2}, {3, 8}, {9, 10}};
      auto actual = mergeOverlappingIntervals(intervals);
      assert(expected == actual);
  }
};

std::vector<std::vector<int>> mergeOverlappingIntervals(std::vector<std::vector<int>> intervals) {
    if (intervals.empty()) {
        return {};
    }

    std::sort(intervals.begin(), intervals.end());
    std::vector<std::vector<int>> result{intervals[0]};

    for (size_t i{1}; i < intervals.size(); ++i) {
        auto& previous = result.back();
        const auto current = intervals[i];

        if (current[0] <= previous[1]) {
            previous[1] = std::max(previous[1], current[1]);
        } else {
            result.push_back(current);
        }
    }

    return result;
}

int main() {
    auto test = Test();
    test.run();

    return 0;
}
