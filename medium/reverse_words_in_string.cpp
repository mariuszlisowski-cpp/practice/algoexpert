#include <iostream>
#include <string>
#include <vector>

std::string reverseWordsInString(std::string str) {
    std::vector<std::string> words;
    std::string word;
    auto letters{true};
    auto it = str.begin();
    while (it != str.end()) {
        if (letters) {
            do {
                if (!std::isspace(*it)) {
                    word += *it;
                } else {
                    words.push_back(word);
                    letters = !letters;
                    word.clear();
                    break;
                }
            } while (++it != str.end());
        }
        if (it == str.end()) {
            words.push_back(word);
            break;
        }
        if (!letters) {
            do {
                if (std::isspace(*it)) {
                    word += *it;
                } else {
                    words.push_back(word);
                    letters = !letters;
                    word.clear();
                    break;
                }
            } while (++it != str.end());
        }
        if (it == str.end()) {
            words.push_back(word);
            break;
        }
    }

    std::string reversed;
    for (auto it = words.rbegin(); it != words.rend(); ++it) {
        reversed += *it;
    }

    return reversed;
}

int main() {
    std::string input = "AlgoExpert  is the best!";
    std::string expected = "best! the is  AlgoExpert";
    
    auto actual = reverseWordsInString(input);
    assert(expected == actual);
}
