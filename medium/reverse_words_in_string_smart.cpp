/*  "AlgoExpert  is the best!" -> reverse each word (and each space by the way)
    "trepxEoglA  si eht !tseb" -> read from the end -> "best! the is  AlgoExpert"
*/
#include <iostream>
#include <string>

std::string reverseWordsInString(const std::string& str) {
    auto reversed{str};
    auto begining{0u};
    for (auto i = 0; i <= str.length(); i++) {
        if (str[i] == ' ' || i == str.length()) {
            std::reverse(reversed.begin() + begining, reversed.begin() + i);
            begining = i + 1;
        }
    }
    std::reverse(reversed.begin(), reversed.end()); // to reverse: "trepxEoglA  si eht !tseb"
    return reversed;
}

int main() {
    std::string input = "AlgoExpert  is the best!";
    std::string expected = "best! the is  AlgoExpert";

    auto actual = reverseWordsInString(input);
    std::cout << actual << std::endl;

    assert(expected == actual);
}
